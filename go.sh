#!/bin/bash
function coloredEcho(){
    local exp=$1;
    local color=$2;
    local mod=$3;
    if ! [[ $color =~ '^[0-9]$' ]] ; then
       case $(echo $color | tr '[:upper:]' '[:lower:]') in
        black) color=0 ;;
        red) color=1 ;;
        green) color=2 ;;
        yellow) color=3 ;;
        blue) color=4 ;;
        magenta) color=5 ;;
        cyan) color=6 ;;
        white|*) color=7 ;; # white or invalid color
       esac
    fi
    tput setaf $color;
    echo $mod "$exp";
    tput sgr0;
}

function installPlugin() {
  local url=$1;
  local directory=$2;
  local path="craft/plugins/$2";

  coloredEcho "Installing $url" white
  git clone --depth=1 --quiet $url $path
  rm -rf "$path/.git"

  if [ -d "$path/$directory" ]; then
    mv $path craft/plugins/tmp
    mv "craft/plugins/tmp/$directory" $path
    rm -rf craft/plugins/tmp
  fi
}

coloredEcho "Type project name (local database suffix AND *.craft.dev prefix): " cyan -n
read projectname

projectnameUpper=$( echo $projectname | tr '[:lower:]' '[:upper:]')

coloredEcho "Type a space separated list of locales (or ENTER for none): " cyan -n
read projectlocales

coloredEcho "Is this a Craft Commerce project? [y/N]: " cyan -n
read projectCommerce

coloredEcho "Download default plugins? [y/N]: " cyan -n
read downloadPlugins

coloredEcho "Download the latest versions of javascript libraries? [y/N]: " cyan -n
read updateJsLibs

echo ''
coloredEcho "Creating database craftcms_$projectname..." green
mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS craftcms_$projectname";
coloredEcho "✔ DONE" white

echo ''
coloredEcho 'Downloading and installing the latest version of Craft...' green

mkdir -p tmp
curl -L http://buildwithcraft.com/latest.zip?accept_license=yes -o tmp/Craft.zip
unzip -q tmp/Craft.zip
rm -rf tmp

rm -rf public
rm -rf craft/templates
find . -name 'web.config' -type f -delete
find . -name 'readme.txt' -type f -delete

if [[ $projectCommerce =~ ^[Yy]$ ]]
then
    echo ''
    coloredEcho 'Downloading and installing the latest version of Craft Commerce...' green
    mkdir -p tmp
    curl -L https://craftcommerce.com/index.php/actions/commerceDownload/go -o tmp/Commerce.zip
    unzip -q tmp/Commerce.zip
    mv commerce craft/plugins/
    rm -rf tmp
    rm -rf templates
fi

coloredEcho "✔ DONE" white

if [[ $downloadPlugins =~ ^[Yy]$ ]]
then
echo ''
coloredEcho 'Downloading and installing default plugins...' green

installPlugin https://github.com/aelvan/Inlin-Craft.git inlin
installPlugin https://github.com/aelvan/Stamp-Craft.git stamp
installPlugin https://github.com/aelvan/Imager-Craft.git imager
installPlugin https://github.com/aelvan/Guano-Craft.git guano
installPlugin https://github.com/aelvan/Preparse-Field-Craft.git preparsefield
installPlugin https://github.com/aelvan/FocalPointField-Craft.git focalpointfield
installPlugin https://github.com/fruitstudios/LinkIt.git fruitlinkit
installPlugin https://github.com/engram-design/ImageResizer.git imageresizer
installPlugin https://github.com/nystudio107/seomatic.git seomatic
installPlugin https://github.com/nystudio107/minify.git minify
installPlugin https://github.com/nystudio107/cookies.git cookies
installPlugin https://github.com/bjerenec/craftcms-blueprint.git blueprint
installPlugin https://github.com/benjamminf/craft-quick-field.git quickfield
installPlugin https://github.com/mmikkel/CpFieldLinks-Craft.git cpfieldlinks
installPlugin https://github.com/Pennebaker/craftcms-thearchitect.git thearchitect

coloredEcho "✔ DONE" white
fi

echo ''
coloredEcho 'Downloading and installing Vrsg Boilerplate...' green

mkdir -p tmp
git clone --depth=1 --quiet git@bitbucket.org:vaersaagod/v-rs-god-boilerplate.git tmp
rm -rf tmp/.git

find ./tmp -type f \( -name "*.php" -o -name "*.json" -o -name "*.conf" \) -exec sed -i '' "s/VRSG_PROJECT_NAME/${projectname}/g" {} \;
find ./tmp -type f \( -name "App.js" \) -exec sed -i '' "s/VRSG_PROJECT_NAME/${projectnameUpper}/g" {} \;
cp -r ./tmp/. ./
rm -rf tmp

mv lpnotify craft/plugins/

# SETTING UP LOCALES IF ANY
for locale in ${projectlocales//,/ }
do
  coloredEcho "Setting up locale $locale..." green
  mkdir public/$locale
  cp -R public/locale/ public/$locale
  find ./public/$locale -type f \( -name "*.php" -o -name "*.htaccess" \) -exec sed -i '' "s/VRSG_PROJECT_LOCALE/${locale}/g" {} \;
done
rm -rf public/locale

rsync -av --quiet config/* craft/config/
rm -rf config

node ./scripts/locales.js "${projectlocales}"
rm scripts/locales.js

coloredEcho "✔ DONE" white

if [[ $updateJsLibs =~ ^[Yy]$ ]]
then
    echo ''
    coloredEcho 'Downloading and installing the latest versions of javascript libraries...' green

    cd public/assets/src/js/libs/
    curl https://raw.githubusercontent.com/bramstein/fontfaceobserver/master/fontfaceobserver.js -s -O
    curl https://raw.githubusercontent.com/aFarkas/lazysizes/master/lazysizes-umd.js -s -O
    curl https://raw.githubusercontent.com/scottjehl/picturefill/master/dist/picturefill.js -s -O
    curl https://raw.githubusercontent.com/mroderick/PubSubJS/master/src/pubsub.js -s -O
    curl https://code.jquery.com/jquery-3.1.1.js -s -o jquery.js
    curl https://raw.githubusercontent.com/requirejs/requirejs/master/require.js -s -o require/require.js
    cd ../../../../../

    mkdir -p tmp
    curl https://github.com/greensock/GreenSock-JS/archive/master.zip -L -o tmp/gsap.zip
    unzip -q -o tmp/gsap.zip
    cp -r ./GreenSock\-JS\-master/src/uncompressed/. ./public/assets/src/js/libs/gsap
    rm -rf ./GreenSock\-JS\-master/
    rm -rf tmp

    coloredEcho "✔ DONE" white
fi

echo ''
coloredEcho 'Setting permission levels...' green
permLevel=774
chmod $permLevel craft/app
chmod $permLevel craft/config
chmod $permLevel craft/storage
coloredEcho "✔ DONE" white

echo ''
coloredEcho 'Now...' green
coloredEcho " 1) Create a host in MAMP for $projectname.craft.dev" magenta
coloredEcho " 2) Run the installer at http://$projectname.craft.dev/admin" magenta
coloredEcho " 3) Type npm install && grunt" magenta
echo ''
coloredEcho 'Make something awesome!' white

echo ''
coloredEcho 'NB! To get LivePreview working, install the lpnotify plugin' cyan